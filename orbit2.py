"""
Python class to create orbits. To be implemented with numpy
"""
from math import cos, sin, pi, radians, degrees, tan, atan, sqrt, ceil, floor
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

earthNameList = ["earth", "Earth"]
sunNameList = ["sun", "Sun", "sol", "Sol"]

# perhaps remove the final 3 arguments
class Orbit:

    def __init__(self, refFrame, semimajorAxis, eccentricity, inclination, angularPosition, argPeriapse):
        self.semimajorAxis = semimajorAxis
        self.eccentricity = eccentricity
        self.inclination = radians(inclination)
        self.angularPosition = radians(angularPosition)
        self.argPeriapse = radians(argPeriapse)
        # gravitational parameter in km^3s^-2
        if refFrame in earthNameList:
            self.gravitationalParameter = 398600.441
        if refFrame in sunNameList:
            self.gravitationalParameter = 132712440000
    
    def radius(self, *args):
        if args[0] == 'a':
            # Pericenter argument
            trueAnomaly = radians(180)
        elif args[0] == 'p':
            # Apocenter argument
            trueAnomaly = 0
        else:
            # if a specific true anomaly is given.
            trueAnomaly = (args[0])
        
        radius = (self.semimajorAxis * (1 - self.eccentricity**2)) / (1 + self.eccentricity * cos(trueAnomaly))

        return radius

    def orbitalPeriod(self):
        """
        returns orbital period in seconds
        """
        period = 2 * pi * (self.semimajorAxis**3/self.gravitationalParameter) ** 0.5
        return period

    def currentAngularPosition(self, time):
        # in radians
        # this asumes constant velocity
        # very wrong
        if time >= self.orbitalPeriod():
            time = time - self.orbitalPeriod()
        newPos = self.angularPosition + (time/self.orbitalPeriod())*2*pi
        return newPos

    def ionCruise(self, deltaV, radius):
        # print(self.semimajorAxis)
        # v = ( (2* self.gravitationalParameter / self.radius(theta) ) - (self.gravitationalParameter/self.semimajorAxis) )**0.5
        v = (self.gravitationalParameter * ( (2 / radius) - (1 / self.semimajorAxis) )) ** 0.5
        # print(self.radius('a'))
        # print(self.radius(theta))
        a = - self.gravitationalParameter / (2 * ( ( ( (v + deltaV) **2) /2) - ( self.gravitationalParameter / radius ) ))
        print("V= " +str(v))
        # print(a)
        self.semimajorAxis = a




    # def xVal(self, theta):
    #     return self.radius(theta) * cos(theta)

    # def yVal(self, theta):
    #     return self.radius(theta) * sin(theta)

    # def eccentricAnomaly(self, theta):
    #     # theta = radians(theta)
    #     upper = tan(theta/2)
    #     lower = sqrt((1 + self.eccentricity) / (1 - self.eccentricity))
    #     E = 2 * atan(upper / lower)
    #     return E
    # def meanAnomaly(self, theta):
    #     return self.eccentricAnomaly(theta) - (self.eccentricity * sin(self.eccentricAnomaly(theta)))
        
    
    # def meanMotion(self):
    #     return sqrt(398600.441/(self.semimajorAxis**3))

    # def tP(self, theta):
    #     tbase = self.meanAnomaly(theta) / self.meanMotion()
    #     if tbase < 0:
    #         t = self.orbitalPeriod() + tbase + (floor(theta / (2 * pi)) * self.orbitalPeriod())
    #     else:
    #         t = tbase + (floor(theta / (2 * pi)) * self.orbitalPeriod())
    #     return t





    # def plotPolar(self):
    #     fig = plt.figure()
    #     theta = np.arange(0, 2*pi, 0.01)
    #     plt.suptitle("Polar plot")
    #     Rfunc = np.vectorize(self.radius)
    #     R = Rfunc(theta)
    #     orb = fig.add_subplot(111, projection="polar")
    #     orb.plot(theta, R, 'r')
    #     orb.set_aspect(1)


# def initPlot():

# def showPlot():
    
#     plt.show()
