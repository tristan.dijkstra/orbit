import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import orbit
from math import pi, ceil

orbits = 9


testorbit = orbit.orbit(42164, 0.2, 180, 0, 0)
earth = orbit.orbit(6378, 0, 0, 0, 0)

# print(testorbit.semimajorAxis)
print(testorbit.radius('a'))
# print(testorbit.orbitalPeriod())
# print(testorbit.tP(1))

theta = np.arange(0, (orbits*2)*pi, 0.01)

Xfunc = np.vectorize(testorbit.xVal)
Yfunc = np.vectorize(testorbit.yVal)
Rfunc = np.vectorize(testorbit.radius)
Xfuncearth = np.vectorize(earth.xVal)
Yfuncearth = np.vectorize(earth.yVal)
Rfuncearth = np.vectorize(earth.radius)

X = Xfunc(theta)
Y = Yfunc(theta)
R = Rfunc(theta)
Xearth = Xfuncearth(theta)
Yearth = Yfuncearth(theta)
Rearth = Rfuncearth(theta)

fig = plt.figure()
plt.suptitle("orbit")

Tfunc = np.vectorize(testorbit.tP)

T = Tfunc(theta)
timetheta = fig.add_subplot(111)
timetheta.plot(T, R, 'r')
# timetheta.set_aspect(1)
# print(T)
# plt.ylabel("Radius of orbit")
# plt.xlabel("time")
# plt.show()

# print(ceil(1 * pi / (2 * pi)))

# Polar
# orbit.initPlot()
testorbit.plotPolar()
earth.plotPolar()
# orbit.showPlot()
plt.show()

# XY
# orb2 = fig.add_subplot(111)
# orb2.plot(X, Y, 'r')
# orb2.set_aspect(1)
# e = fig.add_subplot(111)
# e.plot(Xearth, Yearth,)
# e.set_aspect(1)


# print(R)
# print(theta)
