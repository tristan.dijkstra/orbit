"""
Python class to create orbits. To be implemented with numpy
"""
from math import cos, sin, pi, radians, degrees, tan, atan, sqrt, ceil, floor
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

# perhaps remove the final 3 arguments
class orbit:

    def __init__(self, semimajorAxis, eccentricity, inclination, bigOmega, argPeriapse):
        self.semimajorAxis = semimajorAxis
        self.eccentricity = eccentricity
        self.inclination = radians(inclination)
        self.bigOmega = radians(bigOmega)
        self.argPeriapse = radians(argPeriapse)
    
    def radius(self, *args):
        if args[0] == 'a':
            # Pericenter argument
            trueAnomaly = radians(180)
        elif args[0] == 'p':
            # Apocenter argument
            trueAnomaly = 0
        else:
            # if a specific true anomaly is given.
            trueAnomaly = (args[0])
        
        radius = (self.semimajorAxis * (1 - self.eccentricity**2)) / (1 + self.eccentricity * cos(radians(trueAnomaly)))

        return radius

    def orbitalPeriod(self):
        period = 2 * pi * (self.semimajorAxis**3/398600.441)**0.5
        return period

    def xVal(self, theta):
        return self.radius(theta) * cos(theta)

    def yVal(self, theta):
        return self.radius(theta) * sin(theta)

    def eccentricAnomaly(self, theta):
        # theta = radians(theta)
        upper = tan(theta/2)
        lower = sqrt((1 + self.eccentricity) / (1 - self.eccentricity))
        E = 2 * atan(upper / lower)
        return E
    def meanAnomaly(self, theta):
        return self.eccentricAnomaly(theta) - (self.eccentricity * sin(self.eccentricAnomaly(theta)))
        
    
    def meanMotion(self):
        return sqrt(398600.441/(self.semimajorAxis**3))

    def tP(self, theta):
        tbase = self.meanAnomaly(theta) / self.meanMotion()
        if tbase < 0:
            t = self.orbitalPeriod() + tbase + (floor(theta / (2 * pi)) * self.orbitalPeriod())
        else:
            t = tbase + (floor(theta / (2 * pi)) * self.orbitalPeriod())
        return t





    def plotPolar(self):
        fig = plt.figure()
        theta = np.arange(0, 2*pi, 0.01)
        plt.suptitle("Polar plot")
        Rfunc = np.vectorize(self.radius)
        R = Rfunc(theta)
        orb = fig.add_subplot(111, projection="polar")
        orb.plot(theta, R, 'r')
        orb.set_aspect(1)


# def initPlot():

def showPlot():

    plt.show()
