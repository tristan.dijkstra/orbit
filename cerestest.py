import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import orbit2
from math import pi, ceil, degrees

# https://en.wikipedia.org/wiki/Kepler_orbit#/media/File:Orbit1.svg

# ceres = orbit2.orbit("sun", 414261000, 0.07600902910, 9.2, 30, 73)
ceres = orbit2.Orbit("sun", 149598023, 	0, 1.57869, 30, 73)
# ceres = orbit2.orbit("sun", 10.1338E9, 0.43883, 44.4, 30, 40)

t = 0

r0 = 20
dr = 0.05
dtheta = 0.01
theta = 0
r = ceres.radius(theta)
AU = 149597900 #km

rlist = []
tlist = []
thetalist = []

print("Initial radius: " + str(ceres.radius(theta)/AU) + " AU")
print("Target radius: " + str(414261000/AU) + " AU")

while t < 1 * 31536000 and r < 414261000:
    theta = ceres.currentAngularPosition(t)
    r = ceres.radius(theta)
    rlist.append(r/AU)
    tlist.append(t)
    thetalist.append(theta)
    ceres.ionCruise(0.05, r)
    t += 86400  # 1 day

print("Final time: " + str(t/86400) + " days")
print("Final radius: " + str(r/AU) + " AU")
# print(tlist)
# print(rlist)
# print(thetalist)
# print(ceres.orbitalPeriod()/31536000)
# plt.init()
fig = plt.figure()
plt.suptitle("Polar plot")
orb = fig.add_subplot(111, projection="polar")
orb.plot(thetalist, rlist, 'r')
orb.set_aspect(1)
plt.show()
